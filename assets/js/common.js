$(function() {
    $(document)
        .on("click", ".header-block .interaction", function() {
            var value = $('#earth-block .active').data('key');
            if (typeof value !== 'undefined') {
                $('body').removeClass().addClass(value);
            }
        });

    $('#earth-block').carousel({
        interval: false,
        keyboard: false
    })
});
