var gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    cssmin = require('gulp-cssmin'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    cssnano = require('gulp-cssnano'),
    autoprefixer = require('gulp-autoprefixer'),
    htmlhint = require("gulp-htmlhint"),
    sourcemaps = require('gulp-sourcemaps'),
    plumber = require('gulp-plumber'),
    imageop = require('gulp-image-optimization'),
    scssDest = '/assets/style',
    supported = [
        'last 5 versions',
        'ie >= 10'
    ];

gulp.task('scss', function() {
    gulp.src('./assets/style/*.scss')
        .pipe(sourcemaps.init())
        .pipe(plumber())
        .pipe(sass().on('error', sass.logError))
        .pipe(cssnano({
            autoprefixer: {browsers: supported, cascade: true}
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./assets/style/'))
        .pipe(browserSync.stream());
});

gulp.task('js', function() {
    return gulp.src([
        './assets/js/*.js'
    ])
        .pipe(browserSync.stream());
});
gulp.task('prepareJs', function() {
    return gulp.src([
        './node_modules/jquery/dist/jquery.js',
        './node_modules/bootstrap-sass/assets/javascripts/bootstrap.js',
        './assets/js/**/*.js'
    ])
        .pipe(concat('main.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./assets/js'))
});

gulp.task('htmlhint', function() {
    gulp.src("*.html")
        .pipe(htmlhint({
            "tag-pair": true,
            "style-disabled": true,
            "img-alt-require": true,
            "tagname-lowercase": true,
            "src-not-empty": true,
            "id-unique": true,
            "spec-char-escape": true
        }))
        .pipe(htmlhint.reporter())
});

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: './'
        },
        port: 8080,
        open: false,
        notify: false
    });
    gulp.watch("*.html").on("change", reload);
});

gulp.task('watch', ['browser-sync', 'htmlhint'], function() {
    gulp.watch('./assets/style/*.scss', ['scss']);
    gulp.watch('./assets/js/*.js', ['js']);
    gulp.watch('./*.html');
});
